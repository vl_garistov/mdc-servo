/*
	MDC Servo
	System for manual and automated control of two SACAM SAC1-3380 servo drivers
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

==================================================================================

	File: encoder_test.ino
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: A small support program used during development to verify the correct
	operation of the PLC firmware. This code is meant to run on a separate
	Arduino board and monitor the position feedback from the servo drivers.
*/

#define A_PIN	2	// GREEN
#define B_PIN	3	// RED

static void a_handler(void);

volatile int64_t pos = 0;

// Executes once at startup
void setup()
{
	pinMode(A_PIN, INPUT);
	pinMode(B_PIN, INPUT);
	attachInterrupt(digitalPinToInterrupt(A_PIN), a_handler, RISING);
	Serial.begin(115200);
}

// Executes in an infinite loop after setup()
void loop()
{
	Serial.println((uint32_t) (pos % 10240)); // Old value, most likely wrong
	delay(1000);
}

// Executes at each rising front of A_PIN
static void a_handler(void)
{
	//digitalRead(B_PIN) ? pos-- : pos++;
	pos++;
}
