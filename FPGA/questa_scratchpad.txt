restart -f
add wave  \
sim:/MDC_servo_decoder_tb/MDC_servo_decoder_inst/MAIN_CLK
add wave  \
sim:/MDC_servo_decoder_tb/MDC_servo_decoder_inst/b2v_inst10/mdc_servo_decoder_fsm_0/rst
add wave  \
sim:/MDC_servo_decoder_tb/MDC_servo_decoder_inst/b2v_inst10/mdc_servo_decoder_fsm_0/irq_uart \
sim:/MDC_servo_decoder_tb/MDC_servo_decoder_inst/b2v_inst10/mdc_servo_decoder_fsm_0/servo_fb_state \
sim:/MDC_servo_decoder_tb/MDC_servo_decoder_inst/b2v_inst10/mdc_servo_decoder_fsm_0/servo_fb_next_state \
sim:/MDC_servo_decoder_tb/MDC_servo_decoder_inst/b2v_inst10/mdc_servo_decoder_fsm_0/uart_state \
sim:/MDC_servo_decoder_tb/MDC_servo_decoder_inst/b2v_inst10/mdc_servo_decoder_fsm_0/uart_next_state \
sim:/MDC_servo_decoder_tb/MDC_servo_decoder_inst/b2v_inst10/mdc_servo_decoder_fsm_0/uart_write \
sim:/MDC_servo_decoder_tb/MDC_servo_decoder_inst/b2v_inst10/mdc_servo_decoder_fsm_0/uart_busy \
sim:/MDC_servo_decoder_tb/MDC_servo_decoder_inst/b2v_inst10/mdc_servo_decoder_fsm_0/byte_to_send
add wave  \
sim:/MDC_servo_decoder_tb/MDC_servo_decoder_inst/b2v_inst10/uart_0/the_system_uart_0_regs/control_reg \
sim:/MDC_servo_decoder_tb/MDC_servo_decoder_inst/b2v_inst10/uart_0/the_system_uart_0_regs/status_reg
add wave  \
sim:/MDC_servo_decoder_tb/MDC_servo_decoder_inst/b2v_inst10/uart_0/the_system_uart_0_regs/tx_data
add wave  \
sim:/MDC_servo_decoder_tb/MDC_servo_decoder_inst/b2v_inst10/mdc_servo_decoder_fsm_0/avm_waitrequest
add wave  \
sim:/MDC_servo_decoder_tb/MDC_servo_decoder_inst/b2v_inst10/mdc_servo_decoder_fsm_0/m0_pos \
sim:/MDC_servo_decoder_tb/MDC_servo_decoder_inst/b2v_inst10/mdc_servo_decoder_fsm_0/m1_pos
run -all