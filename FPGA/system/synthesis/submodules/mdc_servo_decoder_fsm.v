// mdc_servo_decoder_fsm.v

// This file was auto-generated as a prototype implementation of a module
// created in component editor.  It ties off all outputs to ground and
// ignores all inputs.  It needs to be edited to make it do something
// useful.
// 
// This file will not be automatically regenerated.  You should check it in
// to your version control system if you want to keep it.

`timescale 1 ps / 1 ps
module mdc_servo_decoder_fsm (
		output wire [7:0]  avm_address,     //     avm.address
		output wire        avm_read,        //        .read
		input  wire [31:0] avm_readdata,    //        .readdata
		output wire        avm_write,       //        .write
		output wire [31:0] avm_writedata,   //        .writedata
		input  wire        avm_waitrequest, //        .waitrequest
		output wire [3:0]  avm_byteenable,  //        .byteenable
		input  wire        ready,           // rot_enc.ready
		input  wire        error,           //        .error
		input  wire [31:0] m0_pos,          //        .m0_pos
		input  wire [31:0] m1_pos,          //        .m1_pos
		input  wire        rst,             //     rst.reset_n
		input  wire        clk,             //     clk.clk
		input  wire        irq_uart         //     IRQ.irq
	);

	// TODO: Auto-generated HDL template

	assign avm_address = 8'b00000000;

	assign avm_read = 1'b0;

	assign avm_byteenable = 4'b0000;

	assign avm_write = 1'b0;

	assign avm_writedata = 32'b00000000000000000000000000000000;

endmodule
