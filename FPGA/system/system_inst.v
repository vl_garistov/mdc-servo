	system u0 (
		.clk_clk        (<connected-to-clk_clk>),        //     clk.clk
		.reset_reset_n  (<connected-to-reset_reset_n>),  //   reset.reset_n
		.rot_enc_ready  (<connected-to-rot_enc_ready>),  // rot_enc.ready
		.rot_enc_error  (<connected-to-rot_enc_error>),  //        .error
		.rot_enc_m0_pos (<connected-to-rot_enc_m0_pos>), //        .m0_pos
		.rot_enc_m1_pos (<connected-to-rot_enc_m1_pos>), //        .m1_pos
		.uart_0_rxd     (<connected-to-uart_0_rxd>),     //  uart_0.rxd
		.uart_0_txd     (<connected-to-uart_0_txd>)      //        .txd
	);

