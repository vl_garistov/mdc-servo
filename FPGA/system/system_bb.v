
module system (
	clk_clk,
	reset_reset_n,
	rot_enc_ready,
	rot_enc_error,
	rot_enc_m0_pos,
	rot_enc_m1_pos,
	uart_0_rxd,
	uart_0_txd);	

	input		clk_clk;
	input		reset_reset_n;
	input		rot_enc_ready;
	input		rot_enc_error;
	input	[31:0]	rot_enc_m0_pos;
	input	[31:0]	rot_enc_m1_pos;
	input		uart_0_rxd;
	output		uart_0_txd;
endmodule
