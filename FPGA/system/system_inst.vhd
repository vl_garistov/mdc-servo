	component system is
		port (
			clk_clk        : in  std_logic                     := 'X';             -- clk
			reset_reset_n  : in  std_logic                     := 'X';             -- reset_n
			rot_enc_ready  : in  std_logic                     := 'X';             -- ready
			rot_enc_error  : in  std_logic                     := 'X';             -- error
			rot_enc_m0_pos : in  std_logic_vector(31 downto 0) := (others => 'X'); -- m0_pos
			rot_enc_m1_pos : in  std_logic_vector(31 downto 0) := (others => 'X'); -- m1_pos
			uart_0_rxd     : in  std_logic                     := 'X';             -- rxd
			uart_0_txd     : out std_logic                                         -- txd
		);
	end component system;

	u0 : component system
		port map (
			clk_clk        => CONNECTED_TO_clk_clk,        --     clk.clk
			reset_reset_n  => CONNECTED_TO_reset_reset_n,  --   reset.reset_n
			rot_enc_ready  => CONNECTED_TO_rot_enc_ready,  -- rot_enc.ready
			rot_enc_error  => CONNECTED_TO_rot_enc_error,  --        .error
			rot_enc_m0_pos => CONNECTED_TO_rot_enc_m0_pos, --        .m0_pos
			rot_enc_m1_pos => CONNECTED_TO_rot_enc_m1_pos, --        .m1_pos
			uart_0_rxd     => CONNECTED_TO_uart_0_rxd,     --  uart_0.rxd
			uart_0_txd     => CONNECTED_TO_uart_0_txd      --        .txd
		);

