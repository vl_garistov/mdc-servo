/*
	MDC Servo
	System for manual and automated control of two SACAM SAC1-3380 servo drivers
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

==================================================================================

	File: MDC_servo_decoder_tb.sv
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Testbench for the servo feedback decoder

	See questa_scratchpad.txt for useful signals that can be observed.
*/

// TODO: Maybe add assertions

timeunit 1ns;
timeprecision 100ps;

// Needed by Questa
`include "MDC_servo_decoder_HDL.v"

`default_nettype	none

module MDC_servo_decoder_tb;

	localparam CLOCK_FRQ = 50_000_000;

	logic clock = 0;
	logic EXT_RESET_N;
	wire M0_A, M0_B, M1_A, M1_B, M0_HOME, M1_HOME;
	logic clk_12M = 0;
	logic rot_dir = 0, run = 0;
	wire UART_TX;

	// Assumes 1ns timeunit
	always #(1_000_000_000 / (CLOCK_FRQ * 2)) clock++;

	always #41667ps clk_12M++;

	initial begin
		EXT_RESET_N = 0;
		#100ns
		EXT_RESET_N = 1;
	end

	initial begin
		repeat(2_000) @(posedge clock);
		rot_dir = 0;
		run = 1;
		#400us
		run = 0;
		#100us
		rot_dir = 1;
		run = 1;
		#500us
		run = 0;
		repeat(2_000) @(posedge clock);
		$stop(2);
	end

	MDC_servo_decoder_HDL MDC_servo_decoder_inst
	(
		.CLK12M(clk_12M),			// input  CLK12M_sig
		.UART_RX(1'b0),				// input  UART_RX_sig
		.MOTOR0_ENC_A(M0_A),		// input  MOTOR0_ENC_A_sig
		.MOTOR0_ENC_B(M0_B),		// input  MOTOR0_ENC_B_sig
		.MOTOR1_ENC_A(M1_A),		// input  MOTOR1_ENC_A_sig
		.MOTOR1_ENC_B(M1_B),		// input  MOTOR1_ENC_B_sig
		.MOTOR0_HOME_POS(M0_HOME),	// input  MOTOR0_HOME_POS_sig
		.MOTOR1_HOME_POS(M1_HOME),	// input  MOTOR1_HOME_POS_sig
		.TEST_LED(),				// output [7:0] TEST_LED_sig
		.UART_TX(UART_TX),			// output  UART_TX_sig
		.EXT_RESET_N(EXT_RESET_N)	// input EXT_RESET_sig
	);
	rotary_sensor_with_home #(.CLOCK_FRQ(CLOCK_FRQ)) motor0_sensor(.clock(clock), .run(run), .rot_dir(rot_dir), .A(M0_A), .B(M0_B), .HOME(M0_HOME));
	rotary_sensor_with_home #(.CLOCK_FRQ(CLOCK_FRQ)) motor1_sensor(.clock(clock), .run(run), .rot_dir(rot_dir), .A(M1_A), .B(M1_B), .HOME(M1_HOME));

endmodule :MDC_servo_decoder_tb

// Emulates the signals of an incremental rotary encoder attached to a rotating motor
module rotary_sensor_with_home #(parameter CLOCK_FRQ) (input wire clock, run, rot_dir, output logic A, B, HOME);

	// BEATS_PER_SECOND = RPM * RESOLUTION * TRANSITIONS_PER_STEP / SECONDS_PER_MINUTE
	localparam BPS = 100 * 16384 * 2 / 60;
	localparam CNT_MAX = CLOCK_FRQ / BPS - 1;

	bit [1:0] gray[4] = {2'b00, 2'b01, 2'b11, 2'b10};
	bit [12:0] pos = 13'h0000;
	int bit_cnt = 0;
	wire bit_clk = bit_cnt == CNT_MAX;
	bit [1:0] gray_cnt = 0;

	always_ff @(posedge clock)
		if (bit_cnt == CNT_MAX)
			bit_cnt <= 0;
		else
			bit_cnt <= bit_cnt + 1;

	always_ff @(posedge clock)
		if (bit_clk && run)
			if (rot_dir == 0) begin
				pos++;
				gray_cnt <= gray_cnt + 1'b1;
			end
			else begin
				pos--;
				gray_cnt <= gray_cnt - 1'b1;
			end

	assign {B, A} = gray[gray_cnt];
	assign HOME = pos == 0;

endmodule :rotary_sensor_with_home
