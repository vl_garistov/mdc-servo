## Generated SDC file "MDC_servo_decoder.out.sdc"

## Copyright (C) 2022  Intel Corporation. All rights reserved.
## Your use of Intel Corporation's design tools, logic functions 
## and other software and tools, and any partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Intel Program License 
## Subscription Agreement, the Intel Quartus Prime License Agreement,
## the Intel FPGA IP License Agreement, or other applicable license
## agreement, including, without limitation, that your use is for
## the sole purpose of programming logic devices manufactured by
## Intel and sold by Intel or its authorized distributors.  Please
## refer to the applicable agreement for further details, at
## https://fpgasoftware.intel.com/eula.


## VENDOR  "Altera"
## PROGRAM "Quartus Prime"
## VERSION "Version 21.1.1 Build 850 06/23/2022 SJ Lite Edition"

## DATE    "Thu Aug 25 12:19:37 2022"

##
## DEVICE  "10M08SAU169C8GES"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {CLK12M} -period 83.333 -waveform { 0.000 41.666 } [get_ports { CLK12M }]


#**************************************************************
# Create Generated Clock
#**************************************************************

create_generated_clock -name {PLL_CLK} -source [get_pins {inst3|altpll_component|auto_generated|pll1|inclk[0]}] -duty_cycle 50/1 -multiply_by 25 -divide_by 3 -master_clock {CLK12M} [get_pins { inst3|altpll_component|auto_generated|pll1|clk[0] }] 
create_generated_clock -name {SLOW_LED_CLK} -source [get_nets {inst2|altclkctrl_0|main_clk_gen_altclkctrl_0_sub_component|wire_clkctrl1_outclk}] -duty_cycle 328911/10000 -multiply_by 1 -divide_by 100000 -master_clock {MAIN_CLK} [get_keepers {led_test_prescaler:inst5|lpm_counter:LPM_COUNTER_component|cntr_kri:auto_generated|counter_reg_bit[26]}] 
create_generated_clock -name {MAIN_CLK} -source [get_pins {inst3|altpll_component|auto_generated|pll1|clk[0]}] -master_clock {PLL_CLK} [get_pins {inst2|altclkctrl_0|main_clk_gen_altclkctrl_0_sub_component|clkctrl1|outclk}] 


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

set_clock_uncertainty -rise_from [get_clocks {MAIN_CLK}] -rise_to [get_clocks {MAIN_CLK}]  0.070  
set_clock_uncertainty -rise_from [get_clocks {MAIN_CLK}] -fall_to [get_clocks {MAIN_CLK}]  0.070  
set_clock_uncertainty -rise_from [get_clocks {MAIN_CLK}] -rise_to [get_clocks {SLOW_LED_CLK}]  0.080  
set_clock_uncertainty -rise_from [get_clocks {MAIN_CLK}] -fall_to [get_clocks {SLOW_LED_CLK}]  0.080  
set_clock_uncertainty -fall_from [get_clocks {MAIN_CLK}] -rise_to [get_clocks {MAIN_CLK}]  0.070  
set_clock_uncertainty -fall_from [get_clocks {MAIN_CLK}] -fall_to [get_clocks {MAIN_CLK}]  0.070  
set_clock_uncertainty -fall_from [get_clocks {MAIN_CLK}] -rise_to [get_clocks {SLOW_LED_CLK}]  0.080  
set_clock_uncertainty -fall_from [get_clocks {MAIN_CLK}] -fall_to [get_clocks {SLOW_LED_CLK}]  0.080  
set_clock_uncertainty -rise_from [get_clocks {SLOW_LED_CLK}] -rise_to [get_clocks {MAIN_CLK}]  0.080  
set_clock_uncertainty -rise_from [get_clocks {SLOW_LED_CLK}] -fall_to [get_clocks {MAIN_CLK}]  0.080  
set_clock_uncertainty -rise_from [get_clocks {SLOW_LED_CLK}] -rise_to [get_clocks {SLOW_LED_CLK}]  0.090  
set_clock_uncertainty -rise_from [get_clocks {SLOW_LED_CLK}] -fall_to [get_clocks {SLOW_LED_CLK}]  0.090  
set_clock_uncertainty -rise_from [get_clocks {SLOW_LED_CLK}] -rise_to [get_clocks {PLL_CLK}]  0.100  
set_clock_uncertainty -rise_from [get_clocks {SLOW_LED_CLK}] -fall_to [get_clocks {PLL_CLK}]  0.100  
set_clock_uncertainty -fall_from [get_clocks {SLOW_LED_CLK}] -rise_to [get_clocks {MAIN_CLK}]  0.080  
set_clock_uncertainty -fall_from [get_clocks {SLOW_LED_CLK}] -fall_to [get_clocks {MAIN_CLK}]  0.080  
set_clock_uncertainty -fall_from [get_clocks {SLOW_LED_CLK}] -rise_to [get_clocks {SLOW_LED_CLK}]  0.090  
set_clock_uncertainty -fall_from [get_clocks {SLOW_LED_CLK}] -fall_to [get_clocks {SLOW_LED_CLK}]  0.090  
set_clock_uncertainty -fall_from [get_clocks {SLOW_LED_CLK}] -rise_to [get_clocks {PLL_CLK}]  0.100  
set_clock_uncertainty -fall_from [get_clocks {SLOW_LED_CLK}] -fall_to [get_clocks {PLL_CLK}]  0.100  
set_clock_uncertainty -rise_from [get_clocks {PLL_CLK}] -rise_to [get_clocks {SLOW_LED_CLK}]  0.100  
set_clock_uncertainty -rise_from [get_clocks {PLL_CLK}] -fall_to [get_clocks {SLOW_LED_CLK}]  0.100  
set_clock_uncertainty -rise_from [get_clocks {PLL_CLK}] -rise_to [get_clocks {PLL_CLK}]  0.090  
set_clock_uncertainty -rise_from [get_clocks {PLL_CLK}] -fall_to [get_clocks {PLL_CLK}]  0.090  
set_clock_uncertainty -fall_from [get_clocks {PLL_CLK}] -rise_to [get_clocks {SLOW_LED_CLK}]  0.100  
set_clock_uncertainty -fall_from [get_clocks {PLL_CLK}] -fall_to [get_clocks {SLOW_LED_CLK}]  0.100  
set_clock_uncertainty -fall_from [get_clocks {PLL_CLK}] -rise_to [get_clocks {PLL_CLK}]  0.090  
set_clock_uncertainty -fall_from [get_clocks {PLL_CLK}] -fall_to [get_clocks {PLL_CLK}]  0.090  


#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************

set_false_path -to [get_keepers {*altera_std_synchronizer:*|din_s1}]
set_false_path -to [get_ports {TEST_LED[0] TEST_LED[1] TEST_LED[2] TEST_LED[3] TEST_LED[4] TEST_LED[5] TEST_LED[6] TEST_LED[7]}]
set_false_path -from [get_ports {MOTOR0_ENC_A MOTOR0_ENC_B MOTOR0_HOME_POS MOTOR1_ENC_A MOTOR1_ENC_B MOTOR1_HOME_POS}] 
set_false_path -to [get_ports {UART_TX}]
set_false_path -from [get_ports {EXT_RESET_N}] 
set_false_path -to [get_pins -nocase -compatibility_mode {*|alt_rst_sync_uq1|altera_reset_synchronizer_int_chain*|clrn}]


#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

