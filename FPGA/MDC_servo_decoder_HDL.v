// Copyright (C) 2022  Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions 
// and other software and tools, and any partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License 
// Subscription Agreement, the Intel Quartus Prime License Agreement,
// the Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is for
// the sole purpose of programming logic devices manufactured by
// Intel and sold by Intel or its authorized distributors.  Please
// refer to the applicable agreement for further details, at
// https://fpgasoftware.intel.com/eula.

// PROGRAM		"Quartus Prime"
// VERSION		"Version 21.1.1 Build 850 06/23/2022 SJ Lite Edition"
// CREATED		"Thu Aug 25 12:08:56 2022"

module MDC_servo_decoder_HDL(
	CLK12M,
	UART_RX,
	MOTOR0_ENC_A,
	MOTOR0_ENC_B,
	MOTOR1_ENC_A,
	MOTOR1_ENC_B,
	MOTOR0_HOME_POS,
	MOTOR1_HOME_POS,
	EXT_RESET_N,
	UART_TX,
	TEST_LED
);


input wire	CLK12M;
input wire	UART_RX;
input wire	MOTOR0_ENC_A;
input wire	MOTOR0_ENC_B;
input wire	MOTOR1_ENC_A;
input wire	MOTOR1_ENC_B;
input wire	MOTOR0_HOME_POS;
input wire	MOTOR1_HOME_POS;
input wire	EXT_RESET_N;
output wire	UART_TX;
output wire	[7:0] TEST_LED;

wire	[26:0] LED_CLK;
wire	MAIN_CLK;
wire	PLL_CLK;
wire	SYNTHESIZED_WIRE_17;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_3;
wire	[31:0] SYNTHESIZED_WIRE_4;
wire	[31:0] SYNTHESIZED_WIRE_5;
wire	SYNTHESIZED_WIRE_18;
wire	SYNTHESIZED_WIRE_10;
wire	SYNTHESIZED_WIRE_12;
wire	SYNTHESIZED_WIRE_13;
wire	SYNTHESIZED_WIRE_14;
wire	SYNTHESIZED_WIRE_15;
wire	SYNTHESIZED_WIRE_16;

assign	SYNTHESIZED_WIRE_18 = 0;




led_test_counter	b2v_inst(
	.clock(LED_CLK[26]),
	.aclr(SYNTHESIZED_WIRE_17),
	.q(TEST_LED));



system	b2v_inst10(
	.clk_clk(MAIN_CLK),
	.reset_reset_n(SYNTHESIZED_WIRE_1),
	.rot_enc_ready(SYNTHESIZED_WIRE_2),
	.rot_enc_error(SYNTHESIZED_WIRE_3),
	.uart_0_rxd(UART_RX),
	.rot_enc_m0_pos(SYNTHESIZED_WIRE_4),
	.rot_enc_m1_pos(SYNTHESIZED_WIRE_5),
	.uart_0_txd(UART_TX));


rot_enc_flt_with_home	b2v_inst11(
	.clock(MAIN_CLK),
	.sclr(SYNTHESIZED_WIRE_17),
	.dir(SYNTHESIZED_WIRE_18),
	.A(MOTOR0_ENC_A),
	.B(MOTOR0_ENC_B),
	.C(MOTOR0_HOME_POS),
	.error(SYNTHESIZED_WIRE_16),
	.ready(SYNTHESIZED_WIRE_13),
	.bidir_counter(SYNTHESIZED_WIRE_4));
	defparam	b2v_inst11.RESOLUTION = 163840;


rot_enc_flt_with_home	b2v_inst12(
	.clock(MAIN_CLK),
	.sclr(SYNTHESIZED_WIRE_17),
	.dir(SYNTHESIZED_WIRE_18),
	.A(MOTOR1_ENC_A),
	.B(MOTOR1_ENC_B),
	.C(MOTOR1_HOME_POS),
	.error(SYNTHESIZED_WIRE_15),
	.ready(SYNTHESIZED_WIRE_14),
	.bidir_counter(SYNTHESIZED_WIRE_5));
	defparam	b2v_inst12.RESOLUTION = 163840;


main_clk_gen	b2v_inst2(
	.inclk(PLL_CLK),
	.outclk(MAIN_CLK));


main_clk_PLL	b2v_inst3(
	.inclk0(CLK12M),
	.c0(PLL_CLK),
	.locked(SYNTHESIZED_WIRE_12));


reset_sync	b2v_inst4(
	.i_clk(MAIN_CLK),
	.i_areset_n(SYNTHESIZED_WIRE_10),
	.o_reset(SYNTHESIZED_WIRE_17),
	.o_reset_n(SYNTHESIZED_WIRE_1));


led_test_prescaler	b2v_inst5(
	.clock(MAIN_CLK),
	.aclr(SYNTHESIZED_WIRE_17),
	.q(LED_CLK));

assign	SYNTHESIZED_WIRE_10 = SYNTHESIZED_WIRE_12 & EXT_RESET_N;

assign	SYNTHESIZED_WIRE_2 = SYNTHESIZED_WIRE_13 & SYNTHESIZED_WIRE_14;

assign	SYNTHESIZED_WIRE_3 = SYNTHESIZED_WIRE_15 | SYNTHESIZED_WIRE_16;


endmodule
