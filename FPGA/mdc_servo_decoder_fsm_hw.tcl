# TCL File Generated by Component Editor 21.1
# Tue Aug 16 12:27:41 EEST 2022
# DO NOT MODIFY


# 
# mdc_servo_decoder_fsm "MDC Servo decoder FSM" v1.1
# Vladimir Garistov 2022.08.16.12:27:41
# Control logic for the MDC Servo decoder project
# 

# 
# request TCL package from ACDS 16.1
# 
package require -exact qsys 16.1


# 
# module mdc_servo_decoder_fsm
# 
set_module_property DESCRIPTION "Control logic for the MDC Servo decoder project"
set_module_property NAME mdc_servo_decoder_fsm
set_module_property VERSION 1.1
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property GROUP "Custom Logic"
set_module_property AUTHOR "Vladimir Garistov"
set_module_property DISPLAY_NAME "MDC Servo decoder FSM"
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property REPORT_HIERARCHY false


# 
# file sets
# 
add_fileset QUARTUS_SYNTH QUARTUS_SYNTH "" ""
set_fileset_property QUARTUS_SYNTH TOP_LEVEL mdc_servo_decoder_fsm
set_fileset_property QUARTUS_SYNTH ENABLE_RELATIVE_INCLUDE_PATHS false
set_fileset_property QUARTUS_SYNTH ENABLE_FILE_OVERWRITE_MODE false
add_fileset_file mdc_servo_decoder_fsm.sv SYSTEM_VERILOG PATH mdc_servo_decoder_fsm.sv TOP_LEVEL_FILE

add_fileset SIM_VERILOG SIM_VERILOG "" ""
set_fileset_property SIM_VERILOG TOP_LEVEL mdc_servo_decoder_fsm
set_fileset_property SIM_VERILOG ENABLE_RELATIVE_INCLUDE_PATHS false
set_fileset_property SIM_VERILOG ENABLE_FILE_OVERWRITE_MODE false
add_fileset_file mdc_servo_decoder_fsm.sv SYSTEM_VERILOG PATH mdc_servo_decoder_fsm.sv


# 
# parameters
# 


# 
# display items
# 


# 
# connection point avm
# 
add_interface avm avalon start
set_interface_property avm addressUnits SYMBOLS
set_interface_property avm associatedClock clk
set_interface_property avm associatedReset rst
set_interface_property avm bitsPerSymbol 8
set_interface_property avm burstOnBurstBoundariesOnly false
set_interface_property avm burstcountUnits WORDS
set_interface_property avm doStreamReads false
set_interface_property avm doStreamWrites false
set_interface_property avm holdTime 0
set_interface_property avm linewrapBursts false
set_interface_property avm maximumPendingReadTransactions 0
set_interface_property avm maximumPendingWriteTransactions 0
set_interface_property avm readLatency 0
set_interface_property avm readWaitTime 1
set_interface_property avm setupTime 0
set_interface_property avm timingUnits Cycles
set_interface_property avm writeWaitTime 0
set_interface_property avm ENABLED true
set_interface_property avm EXPORT_OF ""
set_interface_property avm PORT_NAME_MAP ""
set_interface_property avm CMSIS_SVD_VARIABLES ""
set_interface_property avm SVD_ADDRESS_GROUP ""

add_interface_port avm avm_address address Output 8
add_interface_port avm avm_read read Output 1
add_interface_port avm avm_readdata readdata Input 32
add_interface_port avm avm_write write Output 1
add_interface_port avm avm_writedata writedata Output 32
add_interface_port avm avm_waitrequest waitrequest Input 1
add_interface_port avm avm_byteenable byteenable Output 4


# 
# connection point rot_enc
# 
add_interface rot_enc conduit end
set_interface_property rot_enc associatedClock clk
set_interface_property rot_enc associatedReset rst
set_interface_property rot_enc ENABLED true
set_interface_property rot_enc EXPORT_OF ""
set_interface_property rot_enc PORT_NAME_MAP ""
set_interface_property rot_enc CMSIS_SVD_VARIABLES ""
set_interface_property rot_enc SVD_ADDRESS_GROUP ""

add_interface_port rot_enc ready ready Input 1
add_interface_port rot_enc error error Input 1
add_interface_port rot_enc m0_pos m0_pos Input 32
add_interface_port rot_enc m1_pos m1_pos Input 32


# 
# connection point rst
# 
add_interface rst reset end
set_interface_property rst associatedClock clk
set_interface_property rst synchronousEdges DEASSERT
set_interface_property rst ENABLED true
set_interface_property rst EXPORT_OF ""
set_interface_property rst PORT_NAME_MAP ""
set_interface_property rst CMSIS_SVD_VARIABLES ""
set_interface_property rst SVD_ADDRESS_GROUP ""

add_interface_port rst rst reset_n Input 1


# 
# connection point clk
# 
add_interface clk clock end
set_interface_property clk clockRate 0
set_interface_property clk ENABLED true
set_interface_property clk EXPORT_OF ""
set_interface_property clk PORT_NAME_MAP ""
set_interface_property clk CMSIS_SVD_VARIABLES ""
set_interface_property clk SVD_ADDRESS_GROUP ""

add_interface_port clk clk clk Input 1


# 
# connection point IRQ
# 
add_interface IRQ interrupt start
set_interface_property IRQ associatedAddressablePoint avm
set_interface_property IRQ associatedClock clk
set_interface_property IRQ associatedReset rst
set_interface_property IRQ irqScheme INDIVIDUAL_REQUESTS
set_interface_property IRQ ENABLED true
set_interface_property IRQ EXPORT_OF ""
set_interface_property IRQ PORT_NAME_MAP ""
set_interface_property IRQ CMSIS_SVD_VARIABLES ""
set_interface_property IRQ SVD_ADDRESS_GROUP ""

add_interface_port IRQ irq_uart irq Input 1

