# MDC Servo

System for manual and automated control of two SACAM SAC1-3380 servo drivers. Developed for Mechanical Design and Constructions Ltd.

Released under AGPLv3.

For a detailed explanation see https://gitlab.com/vl_garistov/thesis_tu_mdc-servo.

To turn on the system, all breakers in the electric panel must be closed. The leftmost breaker is the main one.

A touchscreen GUI provides an interface to set 15 user-defined angular positions and angular speed. The positions are in degrees and can vary between -180° and +179.96°. The range does not include +180° to prevent the ambiguity of +/-180° being the same position. An additional position with number 0 and value of 0.00° is present and cannot be edited. The servos can move to positions with 0.035° of precision. The speed is in rotations per minute and can vary between 0.0112 rpm and 16 rpm. It cannot be negative. The GUI also displays the current positions of the two motors.

A set of buttons are used for manual control. The big red button is the emergency shutdown button. The key-switch is used for choosing the operating mode: manual (II), automatic(I) or disabled(0).

In manual mode the motors can be rotated using the + and - buttons to move in anti-clockwise and clockwise directions respectively. The motor select switch determines which motor moves when the buttons for manual control are used. The first motor is selected when the switch is open (horizontal position) and the second motor is selected when the switch is closed (diagonal position).

When the yellow button is pressed, the system is in calibration mode. Both motors must be calibrated after the system is first turned on and after an emergency stop. Automatic movement of uncalibrated motors is not allowed. To indicate that a motor needs calibration, its position is displayed on the screen on a red background instead of the usual light blue background. To calibrate a motor, set the key to manual mode, select a motor and press the calibration button. It starts blinking and remains pressed until it is released by being pressed again. Now move the motor FORWARD until the calibration button stops blinking and the motor stops moving. The motor will slow down noticeably before stopping at its reference point. After this the motor's position on the screen becomes 0.00 and its background becomes light blue.

In automatic mode the system receives commands from an industrial robot over a very simple parallel interface. The robot can order the system to rotate one or the other motor to one of the 16 positions by encoding the position number in 4 bits (simple binary encoding), selecting a motor with an additional bit (0 for the first motor and 1 for the second) and sending a pulse on a control signal to perform the movement. The duration of this pulse should be more than 500ms. The robot receives feedback in the form of two signals that indicate whether or not each motor is currently moving. Please check the electric panel schematics in schematic_and_panel/Schematic_MDC_Servo_2022-08-08.pdf for more information on the interface to the industial robot.
