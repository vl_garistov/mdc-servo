/*
	MDC Servo
	System for manual and automated control of two SACAM SAC1-3380 servo drivers
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

==================================================================================

	File: kuka_test.ino
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: A small support program used during development to simulate inputs
	from a KUKA industrial robot.
*/

// Pins for signals to and from the PLC
#define KUKA_EMSTOP0		R7
#define KUKA_EMSTOP1		I0_0
#define KUKA_MOTOR0_ACT		I0_2
#define KUKA_MOTOR1_ACT		I0_1
#define KUKA_EXECUTE		R3
#define KUKA_MOTOR_SELECT	R1
#define KUKA_MACRO0			R4
#define KUKA_MACRO1			R5
#define KUKA_MACRO2			R6
#define KUKA_MACRO3			R2

static int request_macro(uint8_t motor, uint8_t macro_num);

// Executes once at startup
void setup()
{
	pinMode(KUKA_EMSTOP0, OUTPUT);
	digitalWrite(KUKA_EMSTOP0, HIGH);
	pinMode(KUKA_EMSTOP1, INPUT);
	pinMode(KUKA_MOTOR0_ACT, INPUT);
	pinMode(KUKA_MOTOR1_ACT, INPUT);
	pinMode(KUKA_EXECUTE, OUTPUT);
	digitalWrite(KUKA_EXECUTE, LOW);
	pinMode(KUKA_MOTOR_SELECT, OUTPUT);
	digitalWrite(KUKA_MOTOR_SELECT, LOW);
	pinMode(KUKA_MACRO0, OUTPUT);
	digitalWrite(KUKA_MACRO0, LOW);
	pinMode(KUKA_MACRO1, OUTPUT);
	digitalWrite(KUKA_MACRO1, LOW);
	pinMode(KUKA_MACRO2, OUTPUT);
	digitalWrite(KUKA_MACRO2, LOW);
	pinMode(KUKA_MACRO3, OUTPUT);
	digitalWrite(KUKA_MACRO3, LOW);
}

// Executes in an infinite loop after setup()
void loop()
{
	request_macro(0, 4);
	delay(5000);
}

/*
	Requests from the PLC to execute one of the macros specified on the GUI.
	motor: number of the motor that should perform the maneuver (0 or 1)
	macro_num: The nuber of the macro to be executed (0 to 15 inc.)
	return value: 0 in case of success and a nonzero error code in case of error
*/
static int request_macro(uint8_t motor, uint8_t macro_num)
{
	if (macro_num > 15)
		return -1;
	if (motor > 1)
		return -2;

	digitalWrite(KUKA_MOTOR_SELECT, motor);
	digitalWrite(KUKA_MACRO0, macro_num & 0x01);
	digitalWrite(KUKA_MACRO1, macro_num & 0x02);
	digitalWrite(KUKA_MACRO2, macro_num & 0x04);
	digitalWrite(KUKA_MACRO3, macro_num & 0x08);

	digitalWrite(KUKA_EXECUTE, LOW);
	delay(1000);
	digitalWrite(KUKA_EXECUTE, HIGH);
	delay(1000);
	digitalWrite(KUKA_EXECUTE, LOW);

	return 0;
}
