/*
	MDC Servo
	System for manual and automated control of two SACAM SAC1-3380 servo drivers
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

==================================================================================

	File: motor_macro.cpp
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Simple ring buffer for storing requests to execute macros coming from the KUKA robot

	This allows the robot to rapidly request the execution of multiple macros one after
	the other or to keep the motors always running by requesting the next macro before
	the current one has finished.
*/

#include "motor_macro.h"
#include "dual_servo.h"
#include "Arduino.h"
#include <inttypes.h>

//#define DEBUG

// Create a ring buffer with capacity as the number of elements to be used as a queue
volatile macro_queue_t *mq_create(size_t capacity)
{
	volatile macro_queue_t *mq = (volatile macro_queue_t *) malloc(sizeof(macro_queue_t));
	if (mq == NULL)
		return NULL;

	mq->queue = (volatile motor_macro_t *) malloc(sizeof(motor_macro_t) * capacity);
	if (mq->queue == NULL)
	{
		free((void *) mq);
		return NULL;
	}

	mq->first_macro = 0;
	mq->last_macro = 0;
	mq->capacity = capacity;
	mq->size = 0;

	return mq;
}

// Safely free the memory that was allocated when creating a ring buffer / queue
int mq_destroy(volatile macro_queue_t *mq)
{
	if (mq == NULL)
		return 301;
	cli();
	if (mq->queue == NULL)
	{
		sei();
		return 302;
	}

	free((void *) mq->queue);
	free((void *) mq);
	sei();

	return 0;
}

// Add an element to the back of the queue
int mq_add(volatile macro_queue_t *mq, uint8_t motor, uint8_t macro_num)
{
	if (mq == NULL)
		return 303;
	if (macro_num >= TOTAL_MACROS)
		return 304;
	if (motor > NUMBER_OF_MOTORS - 1)
		return 305;
	cli();
	if (mq->size >= mq->capacity)
	{
		sei();
		return 306;
	}

	mq->queue[mq->last_macro].motor = motor;
	mq->queue[mq->last_macro].macro_num = macro_num;
	mq->last_macro++;
	mq->size++;
	if (mq->last_macro == mq->capacity)
	{
		mq->last_macro = 0;
	}
	sei();

	#ifdef DEBUG
	Serial.println("Added macro");
	Serial.print("size=");
	Serial.print(mq->size);
	Serial.print(", first_macro=");
	Serial.print(mq->first_macro);
	Serial.print(", last_macro=");
	Serial.println(mq->last_macro);
	#endif

	return 0;
}

// Returns a pointer to the first element in the queue but does not advance any counters or pointers
const motor_macro_t *mq_get_first(volatile macro_queue_t *mq)
{
	if (mq == NULL)
		return NULL;
	cli();
	if (mq->queue == NULL || mq->size == 0)
	{
		sei();
		return NULL;
	}

	const motor_macro_t * ptr = (const motor_macro_t *) &(mq->queue[mq->first_macro]);
	sei();

	return ptr;
}

// Move the queue forward by one position and drop the old first element
int mq_advance(volatile macro_queue_t *mq)
{
	if (mq == NULL)
		return 307;
	cli();
	if (mq->queue == NULL)
	{
		sei();
		return 308;
	}
	if (mq->size == 0)
	{
		sei();
		return 309;
	}

	mq->size--;
	mq->first_macro++;
	if (mq->first_macro == mq->capacity)
	{
		mq->first_macro = 0;
	}
	sei();

	#ifdef DEBUG
	Serial.println("Advanced");
	Serial.print("size=");
	Serial.print(mq->size);
	Serial.print(", first_macro=");
	Serial.print(mq->first_macro);
	Serial.print(", last_macro=");
	Serial.println(mq->last_macro);
	#endif

	return 0;
}

// Empty the queue
int mq_clear(volatile macro_queue_t *mq)
{
	if (mq == NULL)
		return 310;

	cli();
	mq->size = 0;
	mq->first_macro = 0;
	mq->last_macro = 0;
	sei();

	return 0;
}

// Returns the current size of the queue
size_t mq_get_size(volatile macro_queue_t *mq)
{
	if (mq == NULL)
		return 0;

	cli();
	size_t mq_size = mq->size;
	sei();

	return mq_size;
}