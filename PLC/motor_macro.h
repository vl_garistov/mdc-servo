/*
	MDC Servo
	System for manual and automated control of two SACAM SAC1-3380 servo drivers
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

==================================================================================

	File: motor_macro.h
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Simple ring buffer for storing requests to execute macros coming from the KUKA robot

	This allows the robot to rapidly request the execution of multiple macros one after
	the other or to keep the motors always running by requesting the next macro before
	the current one has finished.
*/

#ifndef MOTOR_MACRO_H
#define MOTOR_MACRO_H

#include "Arduino.h"
#include <inttypes.h>

#define TOTAL_MACROS	16

// used to implement a queue for commands from KUKA
typedef struct
{
	uint8_t motor;
	uint8_t macro_num;
}
motor_macro_t;

// ring buffer for queueing macro requests from KUKA
typedef struct
{
	size_t first_macro;
	size_t last_macro;
	size_t capacity;
	size_t size;
	volatile motor_macro_t *queue;
}
macro_queue_t;

// Create a ring buffer with capacity as the number of elements to be used as a queue
volatile macro_queue_t *mq_create(size_t capacity);
// Safely free the memory that was allocated when creating a ring buffer / queue
int mq_destroy(volatile macro_queue_t *mq);
// Add an element to the back of the queue
int mq_add(volatile macro_queue_t *mq, uint8_t motor, uint8_t macro_num);
// Returns a pointer to the first element in the queue but does not advance any counters or pointers
const motor_macro_t *mq_get_first(volatile macro_queue_t *mq);
// Move the queue forward by one position and drop the old first element
int mq_advance(volatile macro_queue_t *mq);
// Empty the queue
int mq_clear(volatile macro_queue_t *mq);
// Returns the current size of the queue
size_t mq_get_size(volatile macro_queue_t *mq);

#endif