/*
	MDC Servo
	System for manual and automated control of two SACAM SAC1-3380 servo drivers
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

==================================================================================

	File: buzzer.h
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Functions for controlling the buzzer in the stacklight. UNUSED!
*/

#ifndef BUZZER_H
#define BUZZER_H

#include "Arduino.h"
#include <inttypes.h>

// PLC pins
#define SL_BUZZER			Q2_5

#define MAX_BUZZER_PERIOD	8388	// ms
#define TIM1_TICKS_PER_MS	7.8125

// Initializes the buzzer, must be called in setup()
int buzzer_init(void);
// Make n number of sound pulses with given period and duty cycle
int buzz(uint16_t n, uint16_t period, float duty_cycle);

static inline void buzzer_on(void)
{
	//Clear the counter
	TCNT1 = 0x0;
	//Set clock source to system clock divided by 1024 with prescaler
	TCCR1B = (TCCR1B | (1 << CS10) | (1 << CS12)) & ~(1 << CS11);
}

static inline void buzzer_off(void)
{
	//Set clock source to none
	TCCR1B &= ~((1 << CS10) | (1 << CS11) | (1 << CS12));
}

#endif