/*
	MDC Servo
	System for manual and automated control of two SACAM SAC1-3380 servo drivers
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

==================================================================================

	File: log_error.cpp
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Error reporting helper function
*/

#include "log_error.h"
#include "Arduino.h"

void log_error(int id, const char *msg, bool freeze)
{
	if (freeze)
	{
		cli();
		stop_motor(0);
		stop_motor(1);
		Serial.print("ERROR ");
		Serial.print(id);
		Serial.print(": ");
		Serial.println(msg);
		Serial.flush();
		while (1);
	}
	else
	{
		Serial.print("ERROR ");
		Serial.print(id);
		Serial.print(": ");
		Serial.println(msg);
	}
}
