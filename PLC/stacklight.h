/*
	MDC Servo
	System for manual and automated control of two SACAM SAC1-3380 servo drivers
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

==================================================================================

	File: stacklight.h
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Functions for controlling the stacklight. UNUSED!

	Note: The buzzer module of the stacklight is controlled by buzzer.cpp.
*/

#ifndef STACKLIGHT_H
#define STACKLIGHT_H

#include "Arduino.h"

// PLC pins
#define SL_RED		Q2_0
#define SL_YELLOW	Q2_1
#define SL_GREEN	Q2_2

// possible values of mask parameter when calling set_sl_glow()
typedef enum
{
	SL_GLOW_NONE = 0,
	SL_GLOW_RED = 1,
	SL_GLOW_YELLOW = 2,
	SL_GLOW_GREEN = 4
}
sl_light_mask_t;

// Initializes the stacklight, must be called in setup()
void stacklight_init(void);
/*
	Turn on selected colors of the stacklight.
	mask is a bitwise OR combination of SL_GLOW_RED, SL_GLOW_YELLOW and SL_GLOW_GREEN.
	mask can be SL_GLOW_NONE to turn off all colors.
*/
int set_sl_glow(sl_light_mask_t mask);

#endif