/*
	MDC Servo
	System for manual and automated control of two SACAM SAC1-3380 servo drivers
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published
	by the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

==================================================================================

	File: eeprom_addresses.h
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Addresses for saving data to the EEPROM

	Some of these are not used yet because restoring the calibration and positions
	requires bidirectional communication between the PLC and the FPGA.
*/

#ifndef EEPROM_ADDRESSES_H
#define EEPROM_ADDRESSES_H

#include "dual_servo.h"
#include "nextion_LCD.h"

#define member_size(type, member) sizeof(((type *)0)->member)

#define ADDR_SPEED		0x00
#define ADDR_MACROS		(ADDR_SPEED + member_size(GUI_params_t, speed))
#define ADDR_SAVED		(ADDR_MACROS + member_size(GUI_params_t, macros[0]) * (TOTAL_MACROS - 1))
#define ADDR_REAL_POS	(ADDR_SAVED + sizeof(bool))
#define ADDR_OFFSET		(ADDR_REAL_POS + member_size(dual_servo_t, real_pos))

#endif